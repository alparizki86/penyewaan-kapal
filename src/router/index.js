import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Update from "../views/Update.vue";
import Add from "../views/Add.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/update",
    name: "Update",
    component: Update
  },
  {
    path: "/add-data",
    name: "Add",
    component: Add
  }
];

const router = new VueRouter({
  routes
});

export default router;
